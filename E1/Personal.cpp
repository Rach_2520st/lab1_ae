#include <iostream>
#include<stdio.h>
#include <string>
using namespace std
#include "Personal.h"


Personal::Personal(){

}

void Personal::Ingresar_datos(){
	//se crea una estructura debido a que los datos son de diferente tipo
	
	struct personalUniversidad
	{
		string nombre;
		string sexo;
		int edad;
	};

	personalUniversidad *personalu = new personalUniversidad[20];

	for (int i = 0; i < 20; ++i)
	{
		//ingresa el nombre del usuario, como ocurre mediante un for 
		//este proceso se repite hasta que se complete el total de empleados
		cout << "Ingresa el nombre del usuario:" << i << endl;
		cin << personalu[i].nombre;
		//ocurre igualmente con el sexo
		cout << "Ingrese el sexo del usuario (F o M):" << i << endl;
		cin << personalu[i].sexo;
		//lo mismo ocurre con la edad
		cout << "Ingresa su edad" << i << endl;
		cin << personalu[i].edad;
	}

//el siguiente ciclo permite que las edades se vayan sumando
	for (int j = 0; j < 20; ++j)
	{
		int suma += personalu[j].edad;

	}
	//la siguiente operación genera el promedio de los empleados
	int promedio = (suma/20);
	cout << "El promedio de la edad de los profesionales es: " << promedio << endl;

	int menores;
	int mayores;
	//el siguiente ciclo va revisando una a una las edades y si es menor al promedio se le agrega 1 a
	//menores, si no lo es se le agrega uno a mayores.
	for (int k = 0; k < 20; ++k)
	{
		if (personalu[k] < promedio)
		{
			menores = menores + 1;
		}
		else{
			mayores = mayores + 1;
		}
	}
	cout << "La cantidad de profesionales mayores al promedio es de "  << mayores << endl;
	cout << "La cantidad de profesionales menores al promedio es de " << menores << endl;

 
}

